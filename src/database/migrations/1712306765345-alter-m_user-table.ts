import { MigrationInterface, QueryRunner } from "typeorm";

export class AlterMUserTable1712306765345 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE m_user
            ADD COLUMN m_roles_id CHAR(36) NULL AFTER password;
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE m_user
            DROP COLUMN m_roles_id;
        `);
    }

}
